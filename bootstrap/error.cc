#include <error.hh>
#include <exception>
#include <iostream>

namespace xlang {

void ErrorListener::printError(size_t line, size_t charPositionInLine,
                               std::string_view msg) {
  std::cerr << file << ":" << line << ":" << charPositionInLine + 1 << ": "
            << msg << std::endl;
  has_error = true;
}

ErrorListener::ErrorListener(std::string_view inputfile)
    : file{inputfile}, has_error{false} {}

bool ErrorListener::hasError() {
  return has_error;
}

void ErrorListener::breakTooMany(antlr4::Token *token, size_t num, size_t max) {
  printError(token->getLine(), token->getCharPositionInLine(),
             "tried to break " + std::to_string(num) + " loops, but only " +
                 std::to_string(max) + " surrounding loops");
}

void ErrorListener::breakZero(antlr4::Token *token) {
  printError(token->getLine(), token->getCharPositionInLine(),
             "tried to break zero loops");
}

void ErrorListener::compilerError(const std::string &file, size_t line) {
  std::cerr << "compiler bug in " << file << ":" << line << std::endl;
  std::terminate();
}

void ErrorListener::continueTooMany(antlr4::Token *token, size_t num,
                                    size_t max) {
  printError(token->getLine(), token->getCharPositionInLine(),
             "tried to continue " + std::to_string(num) + " loops, but only " +
                 std::to_string(max) + " surrounding loops");
}

void ErrorListener::continueZero(antlr4::Token *token) {
  printError(token->getLine(), token->getCharPositionInLine(),
             "tried to continue zero loops");
}

void ErrorListener::duplicateFunction(antlr4::Token *token,
                                      const std::string &name) {
  printError(token->getLine(), token->getCharPositionInLine(),
             "duplicate function '" + name + "'");
}

void ErrorListener::loopControlWithoutLoop(antlr4::Token *token) {
  printError(token->getLine(), token->getCharPositionInLine(),
             "loop control statement without loop");
}

void ErrorListener::shadowedVariable(antlr4::Token *token,
                                     const std::string &name) {
  printError(token->getLine(), token->getCharPositionInLine(),
             "definition of variable '" + name +
                 "' shadows previously defined variable");
}

void ErrorListener::syntaxError([[maybe_unused]] antlr4::Recognizer *recognizer,
                                [[maybe_unused]] antlr4::Token *offendingSymbol,
                                size_t line, size_t charPositionInLine,
                                const std::string &msg,
                                [[maybe_unused]] std::exception_ptr e) {
  printError(line, charPositionInLine, msg);
}

void ErrorListener::typeMismatch(antlr4::Token *token, Type expected,
                                 Type actual) {
  printError(token->getLine(), token->getCharPositionInLine(),
             "expected type '" + typeToString(expected) + "', but got '" +
                 typeToString(actual) + "'");
}

void ErrorListener::unknownFunction(antlr4::Token *token,
                                    const std::string &name) {
  printError(token->getLine(), token->getCharPositionInLine(),
             "unknown function '" + name + "'");
}

void ErrorListener::unknownVariable(antlr4::Token *token,
                                    const std::string &name) {
  printError(token->getLine(), token->getCharPositionInLine(),
             "unknown variable '" + name + "'");
}

void ErrorListener::wrongArgumentNumber(antlr4::Token *token,
                                        const std::string &name,
                                        size_t expected, size_t actual) {
  printError(token->getLine(), token->getCharPositionInLine(),
             "function '" + name + "' expects " + std::to_string(expected) +
                 " arguments, but got " + std::to_string(actual));
}

}  // namespace xlang
