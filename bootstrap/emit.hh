#pragma once

#include <fstream>
#include <string>
#include <string_view>
#include <xlangBaseVisitor.h>

namespace xlang {

class EmitVisitor : public xlangBaseVisitor {
  std::ofstream output;
  size_t blockcount;
  size_t tmpcount;
  std::string last;
  std::vector<size_t> loopstack;

  std::string tmp();

 public:
  EmitVisitor(std::string_view outputfile);

  std::any visitFile(xlangParser::FileContext *ctx) override;
  std::any visitFunction(xlangParser::FunctionContext *ctx) override;
  std::any visitStatement(xlangParser::StatementContext *ctx) override;
  std::any visitExpr(xlangParser::ExprContext *ctx) override;
  std::any visitBooleanExpr(xlangParser::BooleanExprContext *ctx) override;
  std::any visitComparisonExpr(
      xlangParser::ComparisonExprContext *ctx) override;
  std::any visitAdditiveExpr(xlangParser::AdditiveExprContext *ctx) override;
  std::any visitMultiplicativeExpr(
      xlangParser::MultiplicativeExprContext *ctx) override;
  std::any visitFactor(xlangParser::FactorContext *ctx) override;
};

}  // namespace xlang
