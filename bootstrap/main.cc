#include <stdlib.h>
#include <unistd.h>

#include <emit.hh>
#include <error.hh>
#include <iostream>
#include <string>
#include <typecheck.hh>
#include <xlangLexer.h>
#include <xlangParser.h>

static void usage() {
  std::cerr << getprogname() << " [-o output.ssa] input.x" << std::endl;
  exit(1);
}

int main(int argc, char **argv) {
  int opt;
  std::string inputfile;
  std::string outputfile;

  while ((opt = getopt(argc, argv, "o:")) != -1) {
    switch (opt) {
      case 'o':
        outputfile = optarg;
        break;
      default:
        usage();
    }
  }
  argc -= optind;
  argv += optind;
  if (argc != 1) {
    usage();
  }
  inputfile = argv[0];
  if (!outputfile.length()) {
    outputfile = "/dev/stdout";
  }

  std::ifstream inputstream{inputfile};
  antlr4::ANTLRInputStream input{inputstream};
  xlang::xlangLexer lexer{&input};
  antlr4::CommonTokenStream tokens{&lexer};
  xlang::xlangParser parser{&tokens};
  xlang::ErrorListener errorlistener{inputfile};

  parser.removeErrorListeners();
  parser.addErrorListener(&errorlistener);
  auto *tree = parser.file();
  if (errorlistener.hasError()) {
    exit(1);
  }

  xlang::TypeCheckVisitor typecheck{errorlistener};
  typecheck.visitFile(tree);
  if (errorlistener.hasError()) {
    exit(1);
  }

  xlang::EmitVisitor emit{outputfile};
  emit.visitFile(tree);
  return 0;
}
