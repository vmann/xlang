#pragma once

#include <BaseErrorListener.h>
#include <string_view>
#include <typecheck.hh>

namespace xlang {

class ErrorListener : public antlr4::BaseErrorListener {
  std::string_view file;
  bool has_error;

  void printError(size_t line, size_t charPositionInLine, std::string_view msg);

 public:
  ErrorListener(std::string_view inputfile);

  bool hasError();

  void breakTooMany(antlr4::Token *token, size_t num, size_t max);
  void breakZero(antlr4::Token *token);
  [[noreturn]] void compilerError(const std::string &file, size_t line);
  void continueTooMany(antlr4::Token *token, size_t num, size_t max);
  void continueZero(antlr4::Token *token);
  void duplicateFunction(antlr4::Token *token, const std::string &name);
  void loopControlWithoutLoop(antlr4::Token *token);
  void shadowedVariable(antlr4::Token *token, const std::string &name);
  void syntaxError(antlr4::Recognizer *recognizer,
                   antlr4::Token *offendingSymbol, size_t line,
                   size_t charPositionInLine, const std::string &msg,
                   std::exception_ptr e) override;
  void typeMismatch(antlr4::Token *token, Type expected, Type actual);
  void unknownFunction(antlr4::Token *token, const std::string &name);
  void unknownVariable(antlr4::Token *token, const std::string &name);
  void wrongArgumentNumber(antlr4::Token *token, const std::string &name,
                           size_t expected, size_t actual);
};

}  // namespace xlang
