grammar xlang;

file  : function+ EOF;
function  : Identifier LeftParen parameterList? RightParen Colon type block;
parameterList  : parameter (Comma parameter)*;
parameter : Identifier Colon type;
type  : TypeInteger
      | TypeBoolean
      ;
block : LeftBrace statement* RightBrace;
statement : If expr block (Else block)?
          | While expr block
          | For expr Semicolon expr Semicolon expr block
          | Break Integer? Semicolon
          | Continue Integer? Semicolon
          | Return expr Semicolon
          | Print expr Semicolon
          | expr Semicolon
          ;
expr  : Identifier assignmentOp expr 
      | booleanExpr
      ;
assignmentOp : Define | Assign | Increment | Decrement;
booleanExpr : comparisonExpr (booleanOp comparisonExpr)*;
booleanOp  : And | Or | Xor;
comparisonExpr : Not comparisonExpr
                | True
                | False
                | additiveExpr (comparisonOp additiveExpr)?
                ;
comparisonOp : Less | LessEqual | Greater | GreaterEqual | Equal | NotEqual;
additiveExpr  : multiplicativeExpr (additiveOp multiplicativeExpr)*;
additiveOp : Plus | Minus | BitAnd | BitOr | BitXor | ShiftLeft | ShiftRight;
multiplicativeExpr  : factor (multiplicativeOp factor)*;
multiplicativeOp : Mul | Div | Rem;
factor  : Minus factor
        | BitNot factor
        | Integer
        | Identifier
        | Identifier LeftParen argumentList? RightParen
        | LeftParen expr RightParen
        ;
argumentList  : expr (Comma expr)*;

TypeInteger : 'Int';
TypeBoolean : 'Bool';
If  : 'if';
Else  : 'else';
While : 'while';
For : 'for';
Break : 'break';
Continue  : 'continue';
Return  : 'return';
Print : 'print';
And : 'and';
Or  : 'or';
Xor : 'xor';
Not : 'not';
True  : 'true';
False : 'false';

LeftParen : '(';
RightParen  : ')';
Colon : ':';
LeftBrace : '{';
RightBrace  : '}';
Define  : ':=';
Assign  : '=';
Less  : '<';
LessEqual : '<=';
Greater : '>';
GreaterEqual  : '>=';
Equal : '==';
NotEqual  : '!=';
Plus  : '+';
Minus : '-';
BitAnd  : '&';
BitOr : '|';
BitXor  : '^';
ShiftLeft : '<<';
ShiftRight  : '>>';
BitNot  : '~';
Mul : '*';
Div : '/';
Rem : '%';
Increment : '=+';
Decrement : '=-';
Comma : ',';
Semicolon : ';';

Identifier  : [a-z][_a-zA-Z0-9]*;
Integer : [0-9]+;

Comment : '//' ~[\n]* '\n' -> skip;
Whitespace  : [ \t\r\n]+ -> skip;
Unknown : .;
