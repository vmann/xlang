#pragma once

#include <scope.hh>
#include <string>
#include <unordered_map>
#include <vector>
#include <xlangBaseVisitor.h>

namespace xlang {

class ErrorListener;

enum class Type
{
  Invalid,
  Integer,
  Boolean,
};

std::string typeToString(Type type);

class TypeCheckVisitor : public xlangBaseVisitor {
  struct Signature {
    Type returntype;
    std::vector<Type> parametertypes;
  };

  ErrorListener &errorlistener;
  std::unordered_map<std::string, Signature> signatures;
  Type returntype;
  Scope<Type> scope;
  size_t loopcount;

 public:
  TypeCheckVisitor(ErrorListener &errorlistener);

  std::any visitFile(xlangParser::FileContext *ctx) override;
  std::any visitFunction(xlangParser::FunctionContext *ctx) override;
  std::any visitType(xlangParser::TypeContext *ctx) override;
  std::any visitBlock(xlangParser::BlockContext *ctx) override;
  std::any visitStatement(xlangParser::StatementContext *ctx) override;
  std::any visitExpr(xlangParser::ExprContext *ctx) override;
  std::any visitBooleanExpr(xlangParser::BooleanExprContext *ctx) override;
  std::any visitComparisonExpr(
      xlangParser::ComparisonExprContext *ctx) override;
  std::any visitAdditiveExpr(xlangParser::AdditiveExprContext *ctx) override;
  std::any visitMultiplicativeExpr(
      xlangParser::MultiplicativeExprContext *ctx) override;
  std::any visitFactor(xlangParser::FactorContext *ctx) override;
};

}  // namespace xlang
