main() : Int {
  for i := 0; i < 10; i =+ 1 {
    print fib_rec(i);
    print fib_iter(i);
  }
}

fib_rec(n : Int) : Int {
  if n < 2 {
    return 1;
  }
  return fib_rec(n - 1) + fib_rec(n - 2);
}

fib_iter(n : Int) : Int {
  x0 := 1;
  x1 := 1;
  for i := 0; i < n; i =+ 1 {
    t := x0 + x1;
    x0 = x1;
    x1 = t;
  }
  return x0;
}
