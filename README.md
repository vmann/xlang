A toy language compiler.

# Build dependencies
 - ANTLR4
 - Meson
 - Ninja
 - C++ compiler

# Runtime dependencies
 - ANTLR4 C++ runtime
 - QBE
 - Assembler (the one in the C compiler will do)

# Build steps
```
meson setup builddir
ninja -C builddir
```
The `xc` binary is in `builddir/bootstrap/xc`. Optionally install it with
```
ninja -C builddir install
```

# Usage
Use `xc` to compile xlang to QBE intermediate langage
```
xc -o foo.ssa foo.x
```
Use `qbe` to optimize and emit assembly
```
qbe -o foo.S foo.ssa
```
Assemble and link the binary (easiest to let the C compiler figure it out)
```
cc -o foo foo.S
```
